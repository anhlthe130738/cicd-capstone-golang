FROM golang:alpine as BUILDER
WORKDIR /go/src/app
COPY . .
RUN go mod vendor
RUN GOOS=linux go build -o lugia ./cmd/server/main.go

FROM alpine
EXPOSE 10000
WORKDIR /app
COPY --from=BUILDER /go/src/app/lugia   /app
COPY --from=BUILDER /go/src/app/configs/* /app/configs/
CMD /app/lugia